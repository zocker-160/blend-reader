<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class blendReaderWithLaunchingBlender300BinaryTest extends TestCase {
    /**
     * @var \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlender300Binary
     */
    private $reader300;
    
    protected function setUp(): void {
        $log = new Logger('blend-reader');
        $log->pushHandler(new StreamHandler('/tmp/blend-reader.log', Logger::DEBUG));

        $this->reader300 = new \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlender300Binary($log,'/opt/blender3.1/blender');
    }
    
    public function testBlender300Compress() {
        $blender_file = __DIR__.'/data/300-compress.blend';
        
        $ret = $this->reader300->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader300->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('300', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
}
